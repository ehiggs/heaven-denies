use heaven_denies;
use heaven_denies::pypi::PypiProxy;
use heaven_denies::telemetry::{get_subscriber, init_subscriber};
use once_cell::sync::Lazy;
use std::net::TcpListener;
use tokio;

static TRACING: Lazy<()> = Lazy::new(|| {
    let default_filter_level = "debug".into();
    let subscriber_name = "test".into();
    if std::env::var("TEST_LOG").is_ok() {
        let subscriber = get_subscriber(subscriber_name, default_filter_level, std::io::stdout);
        init_subscriber(subscriber);
    } else {
        let subscriber = get_subscriber(subscriber_name, default_filter_level, std::io::sink);
        init_subscriber(subscriber);
    }
});

fn spawn_app() -> u16 {
    Lazy::force(&TRACING);
    let listener: TcpListener = TcpListener::bind("127.0.0.1:0").expect("Failed to bind address");
    let port = listener.local_addr().unwrap().port();

    let db_pool = heaven_denies::create_database(":memory:");
    let pypi_proxy = PypiProxy::new(db_pool, "/repository/");
    let server = heaven_denies::run(listener, pypi_proxy).expect("Failed to bind address");
    let _ = tokio::spawn(server);
    port
}

#[tokio::test]
async fn health_check_works() {
    let port = spawn_app();
    let client = reqwest::Client::new();
    let response = client
        .get(format!("http://127.0.0.1:{}/health", port))
        .send()
        .await
        .expect("Failed to execute request.");
    assert!(response.status().is_success());
}
