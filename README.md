# Heaven Denies
Airplane mode for pypi. 
When you're in the sky and have no net access, heaven denies.

# How to use it

Configure the heaven-denies.toml to specify the upstream repo and packages you are interested in mirroring. Then run it using:

```
$ HEAVEN_DENIES_CONFIG_PATH=./heaven-denies.toml ./target/release/heaven-denies | bunyan
```

# How it works
When you start the server it checks the config for the endpoints it needs to serve. It then serves them.

When a request for a package comes in, it will check the db for the metadata. 


# Further Reading
Some other projects of various scope with some similar goals. (And line of code as counted by tokei).
* [bandersnatch](https://pypi.org/project/bandersnatch/) (~9k loc python)
* [devpi](https://pypi.org/project/devpi/) (~33k loc python)
* [pypi-mirror](https://pypi.org/project/python-pypi-mirror/) (~660 loc python) 
* [nexus](https://github.com/sonatype/nexus-public) (240k loc java, 170k loc javascript)
* [romt](https://github.com/drmikehenry/romt) (Rust)
* [panamax-rs](https://github.com/panamax-rs/panamax) (Rust)

## Pypi xmlrpc
* [Warehouse docs](https://warehouse.readthedocs.io/api-reference/xml-rpc.html)
