CREATE TABLE python_release (
  name TEXT NOT NULL COLLATE NOCASE,
  version TEXT NOT NULL,
  file_type TEXT NOT NULL,
  python_version TEXT NOT NULL,
  filename TEXT NOT NULL,
  url TEXT NOT NULL,
  size INTEGER NOT NULL,
  upload_date INTEGER NOT NULL,
  sha256 TEXT NOT NULL,
  md5 TEXT NOT NULL,
  blake2_256 TEXT,
  PRIMARY KEY (name, version, filename)
);

CREATE TABLE python_release_data (
  name TEXT NOT NULL COLLATE NOCASE,
  version TEXT NOT NULL,
  filename TEXT NOT NULL,
  file BLOB NOT NULL,
  PRIMARY KEY (name, version, filename),
  FOREIGN KEY (name, version, filename) REFERENCES python_release(name, version, filename)
);
