extern crate log;
use heaven_denies::config::Config;
use heaven_denies::pypi::PypiProxy;
use std::env;
use std::net::TcpListener;

/*
Actions:
serve - runs the server.
update - pulls the packages and their dependencies
list - lists the packages and their status (downloaded vs not downloaded)
add - adds a package dependency; uses: `heaven-denies add rx==3.2` `heaven-denies add - < requirements.txt`
*/
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let config_path = env::var("HEAVEN_DENIES_CONFIG_PATH").unwrap_or_else(|_| {
        println!("Could not find HEAVEN_DENIES_CONFIG_PATH. Using local dir.");
        "./heaven-denies.toml".into()
    });

    let config_file = std::fs::read_to_string(&config_path).expect("Could not read config file.");
    let configuration = toml::from_str::<Config>(&config_file).expect("Could not parse config file.");

    let log_level = configuration.log_level;

    if configuration.enable_console {
        println!("Enabling Tokio Console metrics");
        console_subscriber::init();
    }

    let subscriber = heaven_denies::telemetry::get_subscriber("heaven-denies".into(), log_level, std::io::stdout);
    heaven_denies::telemetry::init_subscriber(subscriber);

    let db_pool = heaven_denies::create_database(&configuration.database);
    let pypi_proxy = PypiProxy::new(db_pool, &configuration.pypi.upstream);

    let listener = TcpListener::bind(&configuration.host)?;
    let result = heaven_denies::run(listener, pypi_proxy)?.await;

    heaven_denies::telemetry::shutdown();
    result
}
