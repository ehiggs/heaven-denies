use crate::pypi::model;
use crate::pypi::PypiProxy;
use actix_web::{body, get};
use actix_web::{web, Either, HttpRequest, HttpResponse, Responder};
use askama::Template;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Template)]
#[template(path = "pypi/releases.html")]
struct PythonReleases {
    name: String,
    releases: Vec<model::PythonRelease>,
}

impl Responder for PythonReleases {
    type Body = body::BoxBody;
    fn respond_to(self, _req: &HttpRequest) -> HttpResponse {
        match self.render() {
            Ok(body) => {
                // create response and set content type
                HttpResponse::Ok().content_type("text/html").body(body)
            }
            Err(e) => {
                warn!("Could not render PythonRelease: {}", e);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

#[tracing::instrument(skip(proxy))]
#[get("/pypi/{name}/")]
async fn get_package_releases(
    name: web::Path<String>,
    proxy: web::Data<PypiProxy>,
) -> Either<PythonReleases, HttpResponse> {
    let releases = proxy.get_package_releases(&name).await;
    match releases {
        Ok(releases) => Either::Left(PythonReleases {
            name: name.clone(),
            releases,
        }),
        Err(e) => {
            tracing::warn!("Error waiting for metadata: {}", e);
            Either::Right(HttpResponse::InternalServerError().finish())
        }
    }
}

#[tracing::instrument(skip(proxy))]
#[get("/downloads/pypi/{name}/{version}/{filename}")]
async fn download_release(args: web::Path<(String, String, String)>, proxy: web::Data<PypiProxy>) -> HttpResponse {
    let (name, version, filename) = args.into_inner();
    let data = proxy.get_package(&name, &version, &filename).await;
    match data {
        Ok(data) => HttpResponse::Ok().content_type("application/octet-stream").body(data),
        Err(e) => {
            warn!("Error waiting for metadata: {}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(get_package_releases);
    cfg.service(download_release);
}
