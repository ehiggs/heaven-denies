use super::DbPool;
use crate::block_with_tracing;
use crate::schema::{python_release, python_release_data};
use anyhow::Result;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use serde::{Deserialize, Serialize};
use std::sync::Arc;
#[derive(Debug, Clone, Serialize, Deserialize, Insertable, Queryable)]
#[table_name = "python_release"]
pub struct PythonRelease {
    pub name: String,
    pub version: String,
    pub file_type: String,
    pub python_version: String,
    pub filename: String,
    pub url: String,
    pub size: i32,
    pub upload_date: i32, // sadly sqlite does not support datetime and diesel uses i32 for int (sqlx uses i64)
    pub sha256: String,
    pub md5: String,
    pub blake2_256: Option<String>,
}

impl PythonRelease {
    #[tracing::instrument(skip(connection))]
    pub fn insert_one(connection: &SqliteConnection, metadata: &Self) -> Result<()> {
        diesel::insert_into(python_release::table)
            .values(metadata)
            .execute(connection)?;
        Ok(())
    }

    #[tracing::instrument(skip(connection))]
    pub fn insert_many(connection: &SqliteConnection, metadata: &[Self]) -> Result<()> {
        diesel::insert_into(python_release::table)
            .values(metadata)
            .execute(connection)?;
        Ok(())
    }

    #[tracing::instrument(skip(connection))]
    pub fn find_all_by_name(connection: &SqliteConnection, name_needle: &str) -> Result<Vec<Self>> {
        use crate::schema::python_release::dsl::*;
        let result = python_release.filter(name.eq(name_needle)).load(connection)?;
        Ok(result)
    }

    #[tracing::instrument(skip(connection))]
    pub fn find_by_name_version_filename(
        connection: &SqliteConnection,
        name_needle: &str,
        version_needle: &str,
        filename_needle: &str,
    ) -> Result<Option<Self>> {
        use crate::schema::python_release::dsl::*;
        let result = python_release
            .filter(name.eq(name_needle))
            .filter(version.eq(version_needle))
            .filter(filename.eq(filename_needle))
            .first(connection)
            .optional()?;
        Ok(result)
    }

    #[tracing::instrument(skip(db))]
    pub async fn find_by_name_version_filename_async(
        db: &DbPool,
        name_needle: &str,
        version_needle: &str,
        filename_needle: &str,
    ) -> Result<Option<Self>> {
        let conn = db.get()?;
        let name_needle = Box::from(name_needle);
        let version_needle = Box::from(version_needle);
        let filename_needle = Box::from(filename_needle);
        block_with_tracing(move || {
            Self::find_by_name_version_filename(&conn, &name_needle, &version_needle, &filename_needle)
        })
        .await?
    }
}

#[derive(Debug, Serialize, Deserialize, Insertable, Queryable)]
#[table_name = "python_release_data"]
pub struct PythonReleaseData {
    pub name: String,
    pub version: String,
    pub filename: String,
    pub file: Vec<u8>,
}

impl PythonReleaseData {
    #[tracing::instrument(skip(connection))]
    pub fn find_one(
        connection: &SqliteConnection,
        name_needle: &str,
        version_needle: &str,
        filename_needle: &str,
    ) -> Result<Option<Self>> {
        use crate::schema::python_release_data::dsl::*;
        let result: Option<Self> = python_release_data
            .filter(name.eq(&name_needle))
            .filter(version.eq(&version_needle))
            .filter(filename.eq(&filename_needle))
            .first(connection)
            .optional()?;
        Ok(result)
    }

    #[tracing::instrument(skip(db))]
    pub async fn find_one_async(
        db: &DbPool,
        name_needle: &str,
        version_needle: &str,
        filename_needle: &str,
    ) -> Result<Option<Self>> {
        let connection = db.get()?;
        let name_needle = Box::from(name_needle);
        let version_needle = Box::from(version_needle);
        let filename_needle = Box::from(filename_needle);
        block_with_tracing(move || Self::find_one(&connection, &name_needle, &version_needle, &filename_needle)).await?
    }

    #[tracing::instrument(skip(connection, release_data), fields(name=%release_data.name, version=%release_data.version))]
    pub fn insert_one(connection: &SqliteConnection, release_data: &Self) -> Result<()> {
        diesel::insert_into(python_release_data::table)
            .values(release_data)
            .execute(connection)?;
        Ok(())
    }

    #[tracing::instrument(skip(db, release_data), fields(name=%release_data.name, version=%release_data.version))]
    pub async fn insert_one_async(db: &DbPool, release_data: &Arc<Self>) -> Result<()> {
        let connection = db.get()?;
        let release_data: Arc<Self> = release_data.clone();
        block_with_tracing(move || Self::insert_one(&connection, &release_data)).await?
    }

    #[tracing::instrument(skip(connection, release_data))]
    pub fn insert_many(connection: &SqliteConnection, release_data: &[Self]) -> Result<()> {
        diesel::insert_into(python_release_data::table)
            .values(release_data)
            .execute(connection)?;
        Ok(())
    }

    #[tracing::instrument(skip(db, release_data))]
    pub async fn insert_many_async(db: &DbPool, release_data: Box<[Self]>) -> Result<()> {
        let connection = db.get()?;
        block_with_tracing(move || Self::insert_many(&connection, &release_data)).await?
    }
}

/*
impl Responder for PythonReleaseData {
    fn respond_to(self, _req: &HttpRequest) -> HttpResponse {
        let body = serde_json::to_string(&self).unwrap();
        // create response and set content type
        HttpResponse::Ok()
            .content_type("application/json")
            .body(body)
    }
}
*/
