mod model;
pub mod routes;
mod upstream;

use crate::pypi::model::PythonReleaseData;

use super::DbPool;
use crate::block_with_tracing;
use actix_web::web::Bytes;
use anyhow::bail;
use anyhow::Result;
use regex::Regex;
use upstream::PypiClient;
#[derive(Clone)]
pub struct PypiProxy {
    pkg_client: upstream::PypiClient,
    pub db: DbPool, // todo: remove pub access
    upstream: String,
}

lazy_static! {
    static ref SAFE_NAME: Regex = Regex::new(r"[^A-Za-z0-9.]+").unwrap();
}
/// Convert a python package name to a python safe name as described by both setuptools and bandersnatch.
fn safe_name(name: &str) -> String {
    SAFE_NAME.replace_all("-", name).to_lowercase()
}

impl PypiProxy {
    pub fn new(db: DbPool, upstream: &str) -> PypiProxy {
        PypiProxy {
            pkg_client: PypiClient::new(),
            db,
            upstream: String::from(upstream),
        }
    }

    pub async fn get_package_releases(&self, name: &str) -> Result<Vec<model::PythonRelease>> {
        let db_name: String = safe_name(name);
        let connection = self.db.get()?;
        let releases: Vec<model::PythonRelease> = block_with_tracing(move || {
            let releases_res = model::PythonRelease::find_all_by_name(&connection, &db_name);
            releases_res.unwrap_or_else(|err| {
                tracing::info!("Could not load artefacts by name: {}", err);
                vec![]
            })
        })
        .await?;
        if !releases.is_empty() {
            return Ok(releases);
        }
        tracing::info!("No local data found for {}", name);
        let metadata = self.pkg_client.get_project_metadata(name).await?;
        debug!("Downloaded metadata: {:?}", metadata);
        let connection = self.db.get()?;
        let releases: Vec<model::PythonRelease> = metadata.into();
        let releases_to_write = releases.clone(); // Would be ideal to share the same immutable data.
        block_with_tracing(move || {
            let result = model::PythonRelease::insert_many(&connection, &releases_to_write);
            if let Err(e) = result {
                warn!(
                    "Could not insert new releases: {:?}, payload: {:?}",
                    e, releases_to_write
                );
                Err(e)
            } else {
                result
            }
        })
        .await??;
        Ok(releases)
    }

    pub async fn get_package(&self, name: &str, version: &str, filename: &str) -> Result<Bytes> {
        let db_name: Box<str> = Box::from(safe_name(name));
        let release_data_opt = model::PythonReleaseData::find_one_async(&self.db, name, version, filename).await?;
        if let Some(release_data) = release_data_opt {
            tracing::info!("We have the file so we can return it.");
            return Ok(Bytes::from(release_data.file));
        }

        let name_needle = String::from(db_name.clone());
        let version_needle = <&str>::clone(&version);
        let filename_needle = <&str>::clone(&filename);
        let mut metadata_opt = model::PythonRelease::find_by_name_version_filename_async(
            &self.db,
            &name_needle,
            version_needle,
            filename_needle,
        )
        .await?;
        if metadata_opt.is_none() {
            tracing::info!(
                "Couldn't find data for {} {} - {} - loading from source",
                name,
                version,
                filename
            );
            let all_metadata = self.get_package_releases(name).await?;
            metadata_opt = all_metadata.iter().find(|m| m.filename == filename).cloned();
        }

        if metadata_opt.is_none() {
            warn!(
                "Could not find {} {} - {} after going to source",
                name, version, filename
            );
            bail!(
                "Could not find {} {} - {} after going to source",
                name,
                version,
                filename
            );
        }
        let metadata = metadata_opt.unwrap();
        let response_bytes = self.pkg_client.get_download(&metadata.url).await?;

        let release = self.pkg_client.get_project_metadata(&db_name).await?;
        debug!("Downloaded release: {:?}", release);
        let connection = self.db.get()?;
        let release_data = PythonReleaseData {
            name: String::from(db_name),
            version: String::from(version),
            filename: String::from(filename),
            file: response_bytes.to_vec(),
        };
        block_with_tracing(move || {
            model::PythonReleaseData::insert_one(&connection, &release_data)
                .unwrap_or_else(|err| warn!("Could not write file to db: {}", err));
        })
        .await?;
        Ok(response_bytes)
    }
}
