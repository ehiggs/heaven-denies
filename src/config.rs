use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Serialize, Deserialize)]
pub struct Config {
    #[serde(default = "default_host")]
    pub host: String,
    #[serde(default = "default_database")]
    pub database: String,
    pub pypi: PypiConfig,
    #[serde(default = "default_log_level")]
    pub log_level: String, // TODO: make this an enum (or use an enum) for actual log names.
    #[serde(default = "default_enable_console")]
    pub enable_console: bool,
}

fn default_host() -> String {
    "127.0.0.1:8000".to_owned()
}
fn default_database() -> String {
    "cache.db".to_owned()
}

fn default_log_level() -> String {
    "info".into()
}

fn default_enable_console() -> bool {
    false
}

#[derive(Serialize, Deserialize)]
pub enum OperatingSystem {
    Local,
    Source,
    ManyLinux,
    MacOs,
    Windows,
}

#[derive(Serialize, Deserialize)]
pub struct Dependency {
    pub version: String,
}

#[derive(Serialize, Deserialize)]
pub struct PypiConfig {
    #[serde(default = "default_upstream")]
    pub upstream: String,
    #[serde(default = "default_operating_systems")]
    pub os: Vec<OperatingSystem>,
    #[serde(default = "default_python_versions")]
    pub python_versions: Vec<String>,
    #[serde(default = "BTreeMap::new")]
    pub dependencies: BTreeMap<String, Dependency>,
}

fn default_upstream() -> String {
    "https://pypi.org/pypi/".into()
}

fn default_operating_systems() -> Vec<OperatingSystem> {
    vec![OperatingSystem::Local]
}
fn default_python_versions() -> Vec<String> {
    vec!["cp3".to_owned(), "py3".to_owned()]
}
