table! {
    python_release (name, version, filename) {
        name -> Text,
        version -> Text,
        file_type -> Text,
        python_version -> Text,
        filename -> Text,
        url -> Text,
        size -> Integer,
        upload_date -> Integer,
        sha256 -> Text,
        md5 -> Text,
        blake2_256 -> Nullable<Text>,
    }
}

table! {
    python_release_data (name, version, filename) {
        name -> Text,
        version -> Text,
        filename -> Text,
        file -> Binary,
    }
}

allow_tables_to_appear_in_same_query!(python_release, python_release_data,);
