// Bug in clippy/tracing means we need to allow this until this is fixed:
// https://github.com/tokio-rs/tracing/issues/1450
#![allow(clippy::async_yields_async)]
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate crossbeam;

pub type DbPool = diesel::r2d2::Pool<ConnectionManager<SqliteConnection>>;

pub mod config;
pub mod pypi;
mod schema;
pub mod telemetry;

use actix_web::dev::Server;
use actix_web::error::BlockingError;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use anyhow::Result;
use diesel::r2d2::ConnectionManager;
use diesel::sqlite::SqliteConnection;
use std::future::Future;
use std::net::TcpListener;
use tracing_actix_web::TracingLogger;

pub fn block_with_tracing<F, R>(f: F) -> impl Future<Output = Result<R, BlockingError>>
where
    F: FnOnce() -> R + Send + 'static,
    R: Send + 'static,
{
    let current_span = tracing::Span::current();
    web::block(move || current_span.in_scope(f))
}

// default / handler
async fn index() -> impl Responder {
    HttpResponse::Ok().body(
        r#"
        Welcome to heaven-denies.
        Available routes:
        GET /pypi/{name} -> Get download list for package
    "#,
    )
}

async fn health() -> impl Responder {
    HttpResponse::Ok()
}

embed_migrations!();

pub fn create_database(location: &str) -> DbPool {
    let manager = ConnectionManager::<SqliteConnection>::new(location);
    let db_pool = DbPool::builder().build(manager).expect("Could not build db pool");
    embedded_migrations::run(&db_pool.get().unwrap()).expect("Could not create db file.");
    db_pool
}

pub fn run(listener: TcpListener, pypi_proxy: pypi::PypiProxy) -> Result<Server, std::io::Error> {
    tracing::info!("Pulling dependencies");
    // TODO pull dependencies.
    // TODO calculate dependencies,

    let server = HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(pypi_proxy.clone()))
            .wrap(TracingLogger::default())
            .route("/", web::get().to(index))
            .route("/health", web::get().to(health))
            .configure(pypi::routes::configure)
    })
    .listen(listener)?
    .run();
    Ok(server)
}
